package gson;

import bean.DiaosiWithBirthday;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import json.ReadJsonSample;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;

public class GsonReadSample {
    public static void main(String[] args) throws IOException {
        File file = new File(ReadJsonSample.class.getResource("/wangxiaoer.json").getFile());
        String content = FileUtils.readFileToString(file);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        DiaosiWithBirthday wangxiaoer = gson.fromJson(content, DiaosiWithBirthday.class);
//        System.out.println(wangxiaoer.getBirthday().toLocaleString());

        //集合类解析
        System.out.println(wangxiaoer.getMajor());
        System.out.println(wangxiaoer.getMajor().getClass());
    }
}
